# README #

This is project for Social Media Mining class at USU. We use datasets from CIKM 2015 competitions.

### Team members? ###

* Nguyen Vo - nguyenvo@aggiemail.usu.edu
* Sravan Reddy - rsravan94@gmail.com 

### Final Report ###
* [Our report](https://drive.google.com/file/d/0BzJ4lzc40toAbEhXbmNpRkFjQVE/view?usp=sharing)