from blaze.compute.core import compute

__author__ = 'ben'

import operator
import gzip
from os import listdir
from os.path import isfile, join
import json
from pymongo import MongoClient
import pymongo
import networkx as nx
import timeit
from networkx.algorithms import approximation as apxa
import sys, string, os
from subprocess import call
import csv
from bson.objectid import ObjectId
import collections
import  Queue as queue
import  math
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import scipy as sp
import pylab
import community
import hashlib
from os import listdir
from os.path import isfile, join
import re
import shutil
import random
import snap
import time
from datetime import datetime
from matplotlib.transforms import Bbox
from pylab import *
import matplotlib.colors as mcol

def winingTeam():
    '''
    Compute percentage of Winning team as "h" or "a" (cho cac nam separate, + gom chung luon
    '''
    #mid,season,round,tid1,tid2,tid1_loc,tid2_loc,start_dt,venue,att,margin,win_score,win_tid,win_loc,lose_score,lose_loc,lose_tid,h_tid,h_score,h_q1g,h_q2g,h_q3g,h_q4g,h_q1b,h_q2b,h_q3b,h_q4b,a_tid,a_score,a_q1g,a_q2g,a_q3g,a_q4g,a_q1b,a_q2b,a_q3b,a_q4b
    keys = []
    seasons = []
    f = open('./data/seasons.csv', 'rb')
    first = True
    for line in f:
        if first:
            first = False
            keys += line.split(',')
            continue
        args = line.split(',')
        s = {}
        for i in range(len(keys)):
            s[keys[i]] = args[i]
        seasons.append(s)
    winning_home = 0
    winning_away = 0
    winning_due = 0
    w_rate_per_year = {}

    for s in seasons:

        year = s['mid'].split("_")[0]                      #w, d, l
        w_rate_per_year[year] = w_rate_per_year.get(year, [0,0,0])
        w_rate_per_year['all'] = w_rate_per_year.get('all', [0, 0, 0])


        if s['win_loc'] == 'h':
            w_rate_per_year['all'][0] += 1
            w_rate_per_year[year][0] += 1
        elif s['win_loc'] == 'a':
            w_rate_per_year['all'][2] += 1
            w_rate_per_year[year][2] += 1
        elif s['win_loc'] == 'd':
            w_rate_per_year['all'][1] += 1
            w_rate_per_year[year][1] += 1

    print w_rate_per_year

    w_home = []
    due = []
    w_away = []
    # sorted(w_rate_per_year.items(), key=operator )
    map = sorted(w_rate_per_year, key=operator.itemgetter(0))

    map = sorted(w_rate_per_year.iteritems(), key=lambda x: int(x[0]) if x[0] != 'all' else 'all')

    print map
    keys = []
    for e in map:
        k = e[0]
        keys.append(k)
        v = e[1]
        de = sum(v)
        w_home.append(float(v[0]) / de * 100)
        due.append(float(v[1]) /de * 100)
        w_away.append(float(v[2]) / de * 100)
        # print k
    print w_home
    print due
    print w_away

    # propa = np.array([0.13725490196078433, 0.09803921568627451, 0.19801980198019803]) * 100
    # promotions = np.array([0.0392156862745098, 0.0196078431372549, 0]) * 100
    # Spamming = np.array([0.8137254901960784, 0.8725490196078431, 0.7623762376237624]) * 100
    # Celebrity = np.array([0, 0.00980392156862745 , 0.019801980198019802 ]) * 100
    # social_media = np.array([0.00980392156862745, 0, 0.019801980198019802]) * 100
    # plt.subplot(211)
    fig = plt.figure()
    # ax1 = fig.add_subplot(10, 2)
    ax = plt.subplot()
    ind = [i+1 for i in range(len(w_rate_per_year))]
    width = 0.35
    t1 = ax.bar(ind, w_home , width, color='r', label='Home Win')
    t2 = ax.bar(ind, due , width, color='b', label='Due', bottom=w_home)
    t3 = ax.bar(ind, w_away , width, color='c', label='Away Win',bottom=[i+j for i,j in zip(w_home,due)])
    # t4 = ax.bar(ind, Celebrity , width, color='m', label='Celebrity', bottom=[i+j+k for i,j,k in zip(Spamming,propa, promotions)])
    # t5 = ax.bar(ind, social_media , width, color='g', label='Social Media', bottom=[i+j+k+l for i,j,k,l in zip(Spamming,propa, promotions, Celebrity)])


    plt.ylabel('Percent (%)')
    plt.title('Wining Distribution of Australian Football League 2000-2015')
    plt.xticks([e+width/2. for e in ind], keys)
    plt.xlim([min(ind) - width/2., max(ind) + width + width/2.])
    plt.ylim([0, 120])
    plt.yticks(np.arange(0, 110, 10))


    plt.legend(loc=9, bbox_transform=plt.gcf().transFigure, ncol=len(ind), mode='expand', borderaxespad = 0.5)
    plt.grid()
    plt.show()

def genDataTeamSankey():

    #read teamId
    f = open('./data/teams.csv', 'rb')
    first = True
    dict_team = {}
    for line in f:
        if first:
            first = False
            continue
        id, name = line.split(',')
        dict_team[id] = name
    print dict_team

    keys = []
    seasons = []
    f = open('./data/seasons.csv', 'rb')
    first = True
    for line in f:
        if first:
            first = False
            keys += line.split(',')
            continue
        args = line.split(',')
        s = {}
        for i in range(len(keys)):
            s[keys[i]] = args[i]
        seasons.append(s)
    dict_year = {}
    for s in seasons:
        year, round, tid1, tid2 = s['mid'].split('_')
        dict_year[year] = dict_year.get(year, {})
        p1 = 0
        p2 = 0
        if s['win_tid'] == tid1:
            p1 = 4
        elif s['win_tid'] == tid2:
            p2 = 4
        dict_year[year][tid1] = dict_year[year].get(tid1, 0) + p1
        dict_year[year][tid2] = dict_year[year].get(tid2, 0) + p2

    dict__top = {}
    for year, teams in dict_year.iteritems():
        t = sorted(teams.iteritems(), key=lambda x: int(x[1]), reverse=True)

        # real_t = [(dict_team[e[0]], e[1]) for e in t]
        # print year, t
        gap = len(t)/4
        y = int(year) % 100
        if y < 10:
            y = '0%s' % y
        dict__top[year] = [('%sTop4' % y, t[0:gap]), ('%sTop8' % y, t[gap:2*gap]),
                           ('%sTop12' % y, t[2*gap:3*gap]), ('%sRest' % y, t[3*gap:])]


    map = sorted(dict__top.iteritems(), key=lambda x: int(x[0]))
    for e in map:
        print e
    # print map
    graph = []
    years = [2009, 2010, 2011, 2012, 2013, 2014]
    for i in range(len(map)-1):
        # print map[i][0]
        if int(map[i][0]) not in years:
            print map[i][0]
            continue
        left = map[i]
        right = map[i+1]
        # print left
        # print right
        for e1 in left[1]:
            for e2 in right[1]:
                u = e1[0]
                v = e2[0]
                # print u, v
                w = 0
                for team1 in e1[1]:
                    for team2 in e2[1]:
                        if team1[0] == team2[0]:
                            w += 1
                graph.append((u, v, w))
    for e in graph:
        s = '[\'%s\',\'%s\',%s],' %(e[0], e[1], e[2])
        print s

def genDataPlayerSankey():
    #efficient player: How to define? Based on the number of Goals, Behinds / percentage playing time in a season.
    f = open('./data/players.csv')
    first = True
    keys = []
    players_dict = {}
    for line in f:
        if first:
            first = False
            continue
        pid,dob,first_name,last_name,height,weight = line.split(',')
        players_dict[pid] = [first_name + " " + last_name, height, weight, dob]
    f = open('./data/match_stats.csv', 'rb')
    first = True

    #so phut thi dau cho moi tran va so goal, behind moi tran + so assist goal
    dict_year = {}
    ignore_years = ['2000', '2001', '2002'] #no information about this year.
    for line in f:
        if first:
            first = False
            continue
        mid,pid,tid,loc,KI,MK,HB,DI,GL,BH,HO,TK,RB,IF,CL,CG,FF,FA,BR,CP,UP,CM,MI,P,BO,GA,PP = line.split(',')
        year = mid.split('_')[0]
        if year in ignore_years:
            # print year
            continue
        dict_year[year] = dict_year.get(year, {})
        #each match last 80 minutes
        dict_year[year][pid] = dict_year[year].get(pid, [0, 0])
        dict_year[year][pid][0] += int(GA) + int(GL) + int(BH) #sum of goals, behinds and assisst goals
        # print PP
        # if pid == '10265' and year == '2015':
        #     print GA, GL, BH
        #     print line
        dict_year[year][pid][1] += float(PP) * 0.8 #total time played
    dict_year2 = {}
    for year, players in dict_year.iteritems():
        for player, performance in players.iteritems():
            if performance[1] <= 0.000001:
                # print player, performance
                continue

            efficient = performance[0] / float(performance[1])
            dict_year2[year] = dict_year2.get(year, {})
            dict_year2[year][player] = efficient
    # for year, players in dict_year2.iteritems():
    #     print year, players
    dict_top = {}
    for year, players in dict_year2.iteritems():
        p = sorted(players.items(), key=lambda x: x[1], reverse=True)
        gap = len(p) / 4
        t = int(year) % 100
        if t < 10:
            t = '0%s' % t
        dict_top[year] = [('%sA'%t ,p[0:gap]), ('%sB' % t, p[gap:2*gap]), ('%sC' % t, p[2*gap: 3*gap]), ('%sD' % t, p[3*gap:])]

        # print dict_top[year]
    map = sorted(dict_top.items(), key=lambda x: int(x[0]))
    for e in map:
        print e
    graph_sankey = []
    for i in range(len(map)-1):
        if int(map[i][0]) <= 2009:
            continue
        left = map[i]
        right = map[i+1]
        for k1 in left[1]:
            for k2 in right[1]:
                u = k1[0]
                v = k2[0]
                # print u, v
                w = 0
                for e1 in k1[1]:
                    for e2 in k2[1]:
                        if e1[0] == e2[0]:
                            w+=1
                graph_sankey.append((u, v, w))

    for e in graph_sankey:
        s = '[\'%s\', \'%s\', %s],' % (e[0], e[1], e[2])
        print s
    # assert (winning_home + winning_away) == len(seasons)

def genTimeLimeTeam():
    f = open('./data/teams.csv', 'rb')
    first = True
    dict_team = {}
    for line in f:
        if first:
            first = False
            continue
        id, name = line.split(',')
        dict_team[id] = name
    print dict_team

    keys = []
    seasons = []
    f = open('./data/seasons.csv', 'rb')
    first = True
    for line in f:
        if first:
            first = False
            keys += line.split(',')
            continue
        args = line.split(',')
        s = {}
        for i in range(len(keys)):
            s[keys[i]] = args[i]
        seasons.append(s)
    dict_year = {}
    for s in seasons:
        year, round, tid1, tid2 = s['mid'].split('_')
        dict_year[year] = dict_year.get(year, {})
        p1 = 0
        p2 = 0
        if s['win_tid'] == tid1:
            p1 = 4
        elif s['win_tid'] == tid2:
            p2 = 4
        dict_year[year][tid1] = dict_year[year].get(tid1, 0) + p1
        dict_year[year][tid2] = dict_year[year].get(tid2, 0) + p2

    dict__top = {'Rank1': [], 'Rank2': [], 'Rank3': [], 'Rank4': [], 'Rank5': [], 'Rank6': [], 'Rank7': [], 'Rank8': []}
    dict_year_sorted = sorted(dict_year.items(), key=lambda x:int(x[0]))
    # for e in dict_year_sorted:
    #     print e
    for e in dict_year_sorted:
        t = sorted(e[1].iteritems(), key=lambda x: int(x[1]), reverse=True)
        year = e[0]
        for i in range(8):
            key = 'Rank%s' % (i+1)
            list = dict__top[key] # list cac tuple with form (teamID, year_hang)
            curr = t[i] #team can insert vao (teamId, score)


            if len(list) == 0:
                dict__top[key].append((t[i][0], [int(year)]))
            else:
                pre = list[(len(list) - 1)]
                if pre[0] == t[i][0]:
                    dict__top[key][(len(list) - 1)][1].append(int(year))
                else:
                    dict__top[key].append((t[i][0], [int(year)]))

    x = sorted(dict__top.items(), key = lambda x: x[0])
    dict_frequent_top8 = {}

    ddd = {}
    for e in x:
        for cc in e[1]:
            teamId = cc[0]
            teamName = dict_team[teamId]
            teamName = teamName.replace('\n', '')
            years = cc[1]
            dict_frequent_top8[e[0]] = dict_frequent_top8.get(e[0], {})
            dict_frequent_top8[e[0]][teamId] = dict_frequent_top8[e[0]].get(teamId, 0) + len(years)

            ddd[teamId] = 1

            s = '[ \'%s\', \'%s\', new Date(%s, 01, 01), new Date(%s, 12, 30)],' % (e[0], teamName, years[0], years[(len(years) - 1)])
            print s
    map = sorted(dict_frequent_top8.items(), key=lambda x: x[0])
    for (rank, dict_) in map:
        ent = computeEntropy(dict_)
        e = '[\'%s\', %s],' %(rank, ent)
        print e
    #print teams that frequently appear in top8
    ls = [e for e in ddd]
    print ls
def genDataPopularityChart():
    #each team: compute the average of attendants of each season (Area chart).
    f = open('./data/teams.csv', 'rb')
    first = True
    dict_team = {}
    for line in f:
        if first:
            first = False
            continue
        id, name = line.split(',')
        dict_team[id] = name
    print dict_team

    dict_pop = {}
    f = open('./data/seasons.csv', 'rb')
    first = True
    for line in f:
        if first:
            first = False
            continue
        args = line.split(',')
        #mid,season,round,tid1,tid2,tid1_loc,tid2_loc,start_dt,venue,att,margin,win_score,win_tid,win_loc,lose_score,lose_loc,lose_tid,h_tid,h_score,h_q1g,h_q2g,h_q3g,h_q4g,h_q1b,h_q2b,h_q3b,h_q4b,a_tid,a_score,a_q1g,a_q2g,a_q3g,a_q4g,a_q1b,a_q2b,a_q3b,a_q4b
        mid = args[0]
        year = args[1]
        tid1 = args[3]
        tid2 = args[4]
        dict_pop[year] = dict_pop.get(year, {})
        dict_pop[year][tid1] = dict_pop[year].get(tid1, [0, 0])
        t1 = dict_pop[year][tid1]
        t1[0] += int(args[9])
        t1[1] += 1
        dict_pop[year][tid2] = dict_pop[year].get(tid2, [0, 0])
        t2 = dict_pop[year][tid2]
        t2[0] += int(args[9])
        t2[1] += 1
    dict_pop2 = {}
    for year, teams in dict_pop.iteritems():
        for t, p in teams.iteritems():
            pop = float(p[0])/p[1]
            dict_pop2[year] = dict_pop2.get(year, {})
            dict_pop2[year][t] = pop
    map = sorted(dict_pop2.items(), key=lambda x: x[0])
    lsTeamId = ['118', '115', '114', '117', '116', '111', '110', '113', '112', '102', '103', '101', '106', '107', '104', '105']
    next_dict_year = {}
    for (year, dict_) in map:
        next_dict_year[year] = next_dict_year.get(year, {})
        for team, pop in dict_.iteritems():
            if team in lsTeamId:
                next_dict_year[year][team] = pop
    map = sorted(next_dict_year.items(), key=lambda x: x[0])

    for (year, teams) in map:
        ls1 = [year]
        for tid in lsTeamId:
            ls1.append(teams.get(tid, 0))
        s = '\'%s\',' % year
        for e in ls1[1:(len(ls1)-1)]:

            s += '%.2f,' % e
        s += '%.2f' % ls1[len(ls1) - 1]
        xx = '[%s],' % s
        print xx
    t = [dict_team[t].replace('\n', '') for t in lsTeamId]
    t = ['Year'] + t
    print t
    #for each match in season: get atts dict_[year][teamId] = (so att, so match)
def genLineChartNoGoalBehindAssistGoal():
    #Computing the number of goal, assist and behinds of each team over time.
    f = open('./data/teams.csv', 'rb')
    first = True
    dict_team = {}
    for line in f:
        if first:
            first = False
            continue
        id, name = line.split(',')
        dict_team[id] = name
    print dict_team

    dict_pop = {}
    f = open('./data/match_stats.csv', 'rb')
    players_cnt = {}
    first = True
    for line in f:
        if first:
            first = False
            continue
        mid,pid,tid,loc,KI,MK,HB,DI,GL,BH,HO,TK,RB,IF,CL,CG,FF,FA,BR,CP,UP,CM,MI,P,BO,GA,PP = line.split(',')
        year = mid.split('_')[0]
        dict_pop[year] = dict_pop.get(year, {})
        dict_pop[year][tid] = dict_pop[year].get(tid, 0) + int(GL)#sum of goals, behinds and assisst goals

        players_cnt[year] = players_cnt.get(year, {})
        players_cnt[year][tid] = players_cnt[year].get(tid, {})
        players_cnt[year][tid][pid] = players_cnt[year][tid].get(pid, 0)+ int(GL)

    for year, teams in players_cnt.iteritems():
        print year
        for tid, players in teams.iteritems():
            s = sum([cnt for playerId,cnt in players.iteritems()])
            print tid, s

    map = sorted(dict_pop.items(), key=lambda x: x[0])
    for e in map:
        print e
    lsTeamId = ['118', '115', '114', '117', '116', '111', '110', '113', '112', '102', '103', '101', '106', '107', '104', '105']
    # next_dict_year = {}
    # for (year, dict_) in map:
    #     next_dict_year[year] = next_dict_year.get(year, {})
    #     for team, pop in dict_.iteritems():
    #         if team in lsTeamId:
    #             next_dict_year[year][team] = pop
    # map = sorted(next_dict_year.items(), key=lambda x: x[0])

    for (year, teams) in map:
        ls1 = [year]
        for tid in lsTeamId:
            ls1.append(teams.get(tid, 0))
        s = '\'%s\',' % year
        for e in ls1[1:(len(ls1)-1)]:

            s += '%.2f,' % e
        s += '%.2f' % ls1[len(ls1) - 1]
        xx = '[%s],' % s
        print xx
    t = [dict_team[t].replace('\n', '') for t in lsTeamId]
    t = ['Year'] + t
    print t
def computeEntropy(dict_):
    # print dict_
    s = sum([v for k,v in dict_.iteritems()])
    ent = 0
    for k, v in dict_.iteritems():
        p = float(v) / s
        ent += p * math.log(p)
    return -ent

def genCalendarChart():
    from datetime import datetime
    first = True
    f = open('./data/seasons.csv', 'rb')
    dict_date = {}
    for line in f:
        if first:
            first = False
            continue
        args = line.split(',')
        mid = args[0]
        year = mid.split('_')[0]
        d = args[7].split()[0]
        dict_date[d] = dict_date.get(d, [0, 0])
        tt  =dict_date[d]
        tt[0] += 1
        tt[1] += int(args[9])
    first = True
    f = open('./data/finals.csv', 'rb')
    for line in f:
        if first:
            first = False
            continue
        args = line.split(',')
        mid = args[0]
        year = mid.split('_')[0]
        d = args[7].split()[0]
        dict_date[d] = dict_date.get(d, [0, 0])
        tt  =dict_date[d]
        tt[0] += 1
        tt[1] += int(args[9])
    map = sorted(dict_date.items(), key=lambda x: x[0])
    for (d, f) in map:
        density = f[1] / float(f[0])
        t = datetime.strptime(d, '%Y-%m-%d')
        y = t.year
        m = t.month
        dd = t.day
        s = '[new Date(%s, %s, %s), %.2f],' % (y, m, dd, density)
        print s

def predict_outcome():
    import pandas as pd
    from sklearn.svm import SVC

    # Limit the size of the dataframe html output in the ipython notebook.
    pd.set_option("display.max_rows", 10)
    pd.set_option("display.max_columns", 10)
    teams = pd.read_csv("./data/teams.csv")
    seasons = pd.read_csv("./data/seasons.csv")
    unplayed = pd.read_csv("./data/unplayed.csv")

    df_train = seasons[(seasons["season"] >= 2011) & (seasons["season"] <= 2014)].reset_index(drop=True)

    df_train[df_train["margin"] == 0]
    df_train = df_train[df_train["margin"] > 0]
    def get_tid1_prob(r):
        if r["win_tid"] == r["tid1"]:
            prob = 1.0 # tid1 win
        else:
            prob = 0.0 # tid1 loss
        return prob
    df_train["prob"] = df_train.apply(get_tid1_prob, axis=1)

    feat_cols = ["round", "tid1", "tid2", "tid1_loc"]
    df_train = df_train[["mid", "prob"] + feat_cols]
    print df_train
    # df_train["prob"] = df_train.apply(get_tid1_prob, axis=1)
    df_train["round"] = df_train["round"].str.slice(1).astype(int)

    df_train_dum = pd.get_dummies(df_train, columns=["tid1", "tid2", "tid1_loc"])
    # df_train_dum

    X_train = df_train_dum.drop(["mid", "prob"], axis=1)
    y_train = df_train_dum["prob"]

    df_test = (seasons[seasons["season"] == 2015].append(unplayed)[["mid"] + feat_cols])
    df_test["round"] = df_test["round"].str.slice(1).astype(int)
    df_test.reset_index(drop=True, inplace=True)
    # df_test
    df_test_dum = pd.get_dummies(df_test, columns=["tid1", "tid2", "tid1_loc"]).fillna(0)
    # df_test_dum


    clf = SVC(probability=True)
    clf.fit(X_train, y_train)


    X_test = df_test_dum.drop(["mid"], axis=1)
    print X_test

    y_pred = clf.predict_proba(X_test)
    y_pred[:25] # display only the first 25 rows for brevity
    df_submission = pd.DataFrame()

    # Extract the `rid` column (ie. <round>_<tid1>_<tid2>) from the `mid` column of the test set.
    df_submission["rid"] = df_test["mid"].str.slice(5)

    # Assign the predicted `tid1` win probabilities to the `prob` column.
    df_submission["prob"] = y_pred[:,1]

    df_submission.to_csv("sample_submission.csv", index=False)

def countingStat():
    first = True
    f = open('./data/seasons.csv', 'rb')
    dict_t = {}
    for line in f:
        if first:
            first = False
            continue
        args = line.split(',')
        if args[0] == '2015_R14_101_107':
            print "here"
            continue
        year, round, tid1, tid2 = args[0].split('_')
        year = args[1]
        dict_t[year] = dict_t.get(year, {})
        dict_t[year][tid1] = dict_t[year].get(tid1, 0) + 1
        dict_t[year][tid2] = dict_t[year].get(tid2, 0) + 1
    t = [len(teams) for year, teams in dict_t.iteritems() if year == '2015']
    print t

def generateFeatures():
    #compute based on last 2 years.
    #avg goals, behinds, assist, kicks .....
    # avg attent
    #attact score
    #defense score
    training_data = "training_data" #based on year 2014
    DELTA = 0.0001
    dict_t = {}
    first = True
    f = open('./data/seasons.csv', 'rb')
    f1 = open(training_data, 'wb')
    for line in f:
        if first:
            first = False
            continue
        args = line.split(",")
        year = args[1]
        # print year
        if year != '2014':
            # print year
            continue
        tid1 = args[3]
        tid2 = args[4]
        win_tid = args[11]
        lab = 0
        g1 = 0
        l1 = 0
        g2 = 0
        l2 = 0
        win_score = int(args[12])
        loose_score = int(args[14])
        # print "Score: ", win_score, loose_score
        if win_tid == tid1:
            lab = 1 - DELTA
            g1 = win_score
            l1 = loose_score
            g2 = loose_score
            l2 = win_score
        else:
            lab = 0 + DELTA
            g2 = win_score
            l2 = loose_score
            g1 = loose_score
            l1 = win_score
        if year == '2014':
            s = '%s_%s %s\n' %(tid1, tid2, lab)
            f1.write(s)
            # continue
        dict_t[year] = dict_t.get(year, {})    #goals all matches; loose all matches; #matches
        dict_t[year][tid1] = dict_t[year].get(tid1, [0, 0, 0])
        dict_t[year][tid1][0] += g1
        dict_t[year][tid1][1] += l1
        dict_t[year][tid1][2] += 1

        dict_t[year][tid2] = dict_t[year].get(tid2, [0, 0, 0])
        dict_t[year][tid2][0] += g2
        dict_t[year][tid2][1] += l2
        dict_t[year][tid2][2] += 1

    f1.close()
    dict_team_attact_defense = {}
    for year, teams in dict_t.iteritems():
        print year, teams
        for tid, p in teams.iteritems():
            dict_team_attact_defense[tid] = dict_team_attact_defense.get(tid, [0, 0, 0])
            dict_team_attact_defense[tid][0] += p[0]
            dict_team_attact_defense[tid][1] += p[1]
            dict_team_attact_defense[tid][2] += p[2]
    for tid, p in dict_team_attact_defense.iteritems():
        print tid, p
    att_sum = 0
    def_sum = 0
    total_matches = 0
    for tid, p in dict_team_attact_defense.iteritems():
        att_sum += p[0]
        def_sum += p[1]
        total_matches += p[2]
    print "all teams: ", att_sum, def_sum, total_matches
    dd = {}
    for tid, p in dict_team_attact_defense.iteritems():
        dd[tid] = dd.get(tid, [0, 0])
        a1 = (float(p[0])/p[2]) / ((float(att_sum) / total_matches)) #att rate
        d1 = (float(p[1])/p[2]) / ((float(def_sum) / total_matches)) #defense rate
        dd[tid][0] += a1
        dd[tid][1] += d1

    featues = 'features_train.csv'
    f2 = open(featues, 'wb')
    # for tid, scores in dd.iteritems():
    #     s = '%s,%s,%s\n' %(tid, scores[0], scores[1])
    #     f2.write(s)
    #     print tid, scores
    f = open(training_data, 'rb')
    for line in f:
        mid, lab = line.split()
        # print lab
        tid1, tid2 = mid.split('_')
        f2.write('%s %s %s %s %s %s\n' %(mid, dd[tid1][0], dd[tid1][1], dd[tid2][0], dd[tid2][1], lab))
    f2.close()

if __name__ == '__main__':
    # countingStat()
    # genTimeLimeTeam()
    # genDataPopularityChart()
    # genCalendarChart()
    # predict_outcome()
    generateFeatures()